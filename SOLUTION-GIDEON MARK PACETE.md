# Contributor

Hi! My name is **Gideon Mark Pacete** and my email is **gideonmarkpacete@gmail.com**.


# Links

## AWS EC2

### Backend
> The API key is required for the GET methods. Please use `x-api-key` for the headers.

|  METHOD| URL | API KEY | DESCRIPTION |
|--|--|--|--|
| GET | http://3.101.23.51/api | SOM3V@L1D-@P1-K3Y | List of User Names |
| GET | http://3.101.23.51/api/user/:id | SOM3V@L1D-@P1-K3Y | User by ID |
| POST | http://3.101.23.51/api/user |  | Form Submission |


### Frontend
> Please use http://3.101.23.51/api/user for the form action

FORM
http://54.153.20.61/tht-frontend/


## GitLab Repositories

| TYPE | URL | DESCRIPTION |
|--|--|--|
| MAIN | https://gitlab.com/gideonmarked/tht |  This contains the directories backend and frontend |
| BACKEND | https://gitlab.com/gideonmarked/tht-backend | This contains the script for backend including the database. Docker configuration is used |
| FRONTEND | https://gitlab.com/gideonmarked/tht-frontend| This contains the frontend that has the form for creating users |

## AWS Credentials

Login Link:
https://161321258463.signin.aws.amazon.com/console
    
Username:
Tester
    
Password:

    T3$tingP@$$



# Local Setup
## Requirements
**Docker**
https://www.docker.com/products/docker-desktop/

**Node and NPM**
https://nodejs.org/en/

> If you are using Windows, you can download Git bash for ease of use for terminal commands.
> Here's the link, https://git-scm.com/downloads.
---
### Cloning the Repositories
**Backend**
Clone the main repo to your preferred directory location. You can get the main repo here [https://gitlab.com/gideonmarked/tht](https://gitlab.com/gideonmarked/tht)

Using a terminal navigate to the directory that you have cloned. First navigate to `backend/` directory and run it in the terminal using docker commands.

    cd <repo-folder>/backend
    docker-compose up --build -d

> The port used by the `api/` is `80:80` and the port used by the `database/` is `3306:3306`.
> You are able to change the ports inside the `docker-compose.yml` which is found in `<repo-folder>/backend/docker-compose.yml`. It will look something like this:

    version: '3.1'
    services:
      api:
        build: ./api
        ports:
         - "80:80"
        depends_on:
         - db
        environment:
         - DATABASE_HOST=db
         - API_KEY=SOM3V@L1D-@P1-K3Y
        volumes:
         - ./api:/app/api
      db:
        build: ./database
        command: --default-authentication-plugin=mysql_native_password
        restart: always
        ports:
          - 3306:3306

  **Frontend**
Navigate to `frontend/` directory you can `double-click` `index.html` .

    cd <repo-folder>/frontend

You can change the action form value when needed inside the `index.html` file. You can edit it using notepad or any text editor your prefer. The action value may look something like this:

    <h2> create a new user</h2> 
            <form id="form" action="post-url-for-submission">
              <div class="field-group">
You may replace the `post-url-for-submission` with `http://localhost/api/user` or `http://localhost:<api-port>/api/user` if you have changed the port for the api. The default value is pointed to the one being used in AWS.


# AWS Setup

The setup AWS uses 2 separate containers for backend and frontend respectively.
> This guide assumes that you have connected to each container using ssh with a *.pem key
## Backend-End Setup

### Install and Enable Docker
---
1. Do yum updates
2. Install Docker
3. Start docker service
4. Enable docker in `systemctl` so it would run automatically at startup
5. Allow docker to be run even if not in sudo user
---
	sudo yum update -y
    sudo amazon-linux-extras install docker
    sudo service docker start
    sudo systemctl enable docker
    sudo usermod -a -G docker ec2-user

### Install docker compose

    sudo curl -L https://github.com/docker/compose/releases/latest/download/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose
    sudo chmod +x /usr/local/bin/docker-compose
    docker-compose version
    
### Install Git

    sudo yum update -y
	sudo yum install git -y
	git version

### Clone backend repo

    clone git https://gitlab.com/gideonmarked/tht-backend.git
    cd tht-backend

### Run backend using docker-compose

    docker-compose up --build -d

## Backend-End Setup
### Install and Enable nginx

    sudo amazon-linux-extras install -y nginx1
	sudo systemctl start nginx

### Install Git

    sudo yum update -y
	sudo yum install git -y
	git version
	
### Clone the repo and allow permissions

    cd /usr/share/nginx/
    sudo chmod -R 777 html/
    cd /usr/share/nginx/html/
    git clone https://gitlab.com/gideonmarked/tht-frontend.git
    sudo chmod -R 777 tht-frontend/

You should be able to access the the url in the browser. It may look similar to this:

    http://<public-ip-address-of-fronend-ec2>/tht-frontend/

Don't forget to change the url of the action for the form in `index.html` file.

    <h2> create a new user</h2> 
            <form id="form" action="http://post-url-for-submission/api/users">
              <div class="field-group">


# Notes
Everything should be up and running. Cheers!
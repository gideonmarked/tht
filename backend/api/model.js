const database = require("./database");

//Initialize User values
const User = function(user) {
  this.StudentId = user.StudentId;
  this.FullName = user.FullName;
  this.Email = user.Email;
};

//Method used to create new user. User object is passed
User.create = (newUser, result) => {

    //Create new user with set values
    database.query("INSERT INTO users SET ?", newUser, (err, res) => {
        if (err) {
          console.log("error: ", err);
          result(err, null);
          return;
        }
        result(null, { StudentId: res.insertId, ...newUser });
    });
};

//Method used to retrieve all user from the user table
User.getAll = (result) => {
  let query = "SELECT * FROM users";
  database.query(query, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    result(null, res);
  });
};

//Method used to retrieve a user with id identifier from the user table
User.findById = (id, result) => {
  database.query(`SELECT * FROM users WHERE StudentId = ${id}`, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }
    if (res.length) {
      result(null, res[0]);
      return;
    }
    // not found Tutorial with the id
    result({ kind: "not_found" }, null);
  });
};


module.exports = User;
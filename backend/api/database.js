// const mysql = require('mysql2/promise');
const mysql = require("mysql");
const config = require('./config');

// async function query(sql, params) {
//   const connection = await mysql.createConnection(config.db);
//   const [results, fields] = await connection.execute(sql, params);

//   return results;
// }

// module.exports = {
//   query
// }


const connection = mysql.createConnection(config.db);
// open the MySQL connection
connection.connect(error => {
  if (error) throw error;
  console.log("Successfully connected to the database.");
});
module.exports = connection;
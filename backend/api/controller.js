const User = require("./model.js");
// Create and Save a new User
exports.create = (req, res) => {
  //Return error 400 if no fields were set
  if (!req.body) {
    res.status(400).send({
      message: "Content can not be empty!"
    });
  }

  //Retrieve the values from the form fields
  const FullName = req.body.full_name;
  const Email = req.body.email;

  //Create new User model(model.js) and set the values
  const user = new User({
    StudentId: null,
    FullName: FullName,
    Email: Email
  });

  //Call create method from model.js and pass the user object
  User.create(user, (err, data) => {
    //Return error if issues are found
    if (err)
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the User."
      });
    //return the user object when successful
    else res.send(data);
  });
};

// Retrieve all Users from the database (without condition).
exports.findAll = (req, res) => {
  User.getAll((err, data) => {
    if (err)
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving users."
      });
    else res.send(data);
  });
};

// Retrieve all Users' full name from the database (without condition).
exports.findAllNames = (req, res) => {

    //Get api key from the authorization header
    const apiKey = req.header('x-api-key');

    //Call the getAll method from model.js
    User.getAll((err, data) => {
        if (err)
          res.status(500).send({
            message:
              err.message || "Some error occurred while retrieving users."
          });
        else
        {
            const names = data.map(({FullName})=>({FullName}));
            res.send(names);
        }
    });
};

// Find a single User with a :id
exports.findOne = (req, res) => {

    //Get api key from the authorization header
    const apiKey = req.header('x-api-key');

    //Call the findById method from model.js which requests an <integer>id value
    User.findById(req.params.id, (err, data) => {
        if (err) {
          if (err.kind === "not_found") {
            res.status(404).send({
              message: `Not found User with id ${req.params.id}.`
            });
          } else {
            res.status(500).send({
              message: "Error retrieving User with id " + req.params.id
            });
          }
        } else res.send(data);
    });
};

// Update a User identified by the id in the request
exports.update = (req, res) => {
  
};
// Delete a User with the specified id in the request
exports.delete = (req, res) => {
  
};
// Delete all Users from the database.
exports.deleteAll = (req, res) => {
  
};